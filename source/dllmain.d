import core.sys.windows.windows, core.sys.windows.dll;
import std.concurrency : spawn, yield;
import std.file : exists, isFile;
import std.path : dirName;
import core.runtime;

import llmo, inifiled;

__gshared HINSTANCE g_hinst;

extern (Windows) BOOL DllMain(HINSTANCE hInstance, ULONG ulReason, LPVOID)
{
	g_hinst = hInstance;
	final switch (ulReason)
	{
	case DLL_PROCESS_ATTACH:
		Runtime.initialize();
		spawn({
			while (*cast(uint*) 0xC8D4C0 < 9)
				yield();
			init();
		});
		dll_process_attach(hInstance, true);
		break;
	case DLL_PROCESS_DETACH:
		uninit();
		Runtime.terminate();
		dll_process_detach(hInstance, true);
		break;
	case DLL_THREAD_ATTACH:
		dll_thread_attach(true, true);
		break;
	case DLL_THREAD_DETACH:
		dll_thread_detach(true, true);
		break;
	}
	return true;
}

/// Uninitialize plugin: restore original code
void uninit()
{
	// Enable background
	writeMemory(0x57B7CC, [0x0F, 0x84], MemorySafe.safe);
	writeMemory(0x57B9BB, [0xE8, 0xA0, 0xC1, 0x1A, 0x00], MemorySafe.safe);
	writeMemory!ubyte(0x576E14, 0xFF, MemorySafe.safe);
	writeMemory!ubyte(0x576E29, 0xFF, MemorySafe.safe);
	writeMemory!ubyte(0x57B9CA, 0x74, MemorySafe.safe);

	// Enable background for map
	writeMemory(0x57549D, [0xE8, 0xBE, 0x26, 0x1B, 0x00], MemorySafe.safe);
	writeMemory(0x575458, [0xE8, 0xF2, 0x26, 0x1B, 0x00], MemorySafe.safe);

	// Disable render world
	writeMemory(0x53E9B3, [0x0F, 0x85, 0x60, 0x01, 0x00, 0x00], MemorySafe.safe);

	// Restore pause
	writeMemory!ubyte(0x561AF0, 0xC6);

	// Remove hook
	writeMemory(0x53E9F9, [0xE8, 0xE2, 0x6C, 0x01, 0x00], MemorySafe.safe);
}

/// Initialize plugin: set hacks and load settings
void init()
{
	// Load settings
	char[260] path;
	GetModuleFileNameA(g_hinst, path.ptr, 260);
	Config cfg;
	auto cfgFile = cast(string) path.dirName ~ "/TransparentMenu.ini";
	if (cfgFile.exists && cfgFile.isFile)
		cfg.readINIFile(cfgFile);
	else
		cfg.writeINIFile(cfgFile);

	// Disable background
	writeMemory(0x57B7CC, [0x90, 0xE9], MemorySafe.safe);
	setMemory(0x57B9BB, 0x90, 5, MemorySafe.safe);
	writeMemory!ubyte(0x576E14, 0x00, MemorySafe.safe);
	writeMemory!ubyte(0x576E29, 0x00, MemorySafe.safe);
	writeMemory!ubyte(0x57B9CA, 0xEB, MemorySafe.safe);

	// Disable background for map
	if (cfg.map.trans)
	{
		setMemory(0x57549D, 0x90, 5, MemorySafe.safe);
		setMemory(0x575469, 0x90, 5, MemorySafe.safe);
	}

	// Render world
	writeMemory(0x53E9B3, [0x75, 0x44, 0x90, 0x90, 0x90, 0x90], MemorySafe.safe);

	// No pause
	if (!cfg.pause)
		writeMemory!ubyte(0x561AF0, 0xC3);

	// Hook render 2D stuff
	auto hook = callToNull;
	immutable hookCallerOffset = hook.length;
	hook ~= callToNull;
	hook ~= callReturn;

	auto hookCaller = cast(ubyte*)(cast(size_t) hook.ptr + hookCallerOffset);

	fixNull(hook.ptr, cast(void*) 0x5556E0);
	fixNull(hookCaller, &render2dStuff);

	setMemoryProtection(hook.ptr, hook.length);
	simpleCallHook(0x53E9F9, cast(void*) hook.ptr, 5);
}

/// Hook of Idle for enable/disable game function render2dstuff
extern (Windows) void render2dStuff()
{
	__gshared ubyte[] bak;
	__gshared ubyte oldMenuId;
	__gshared ubyte oldState;
	immutable menuId = readMemory!ubyte(cast(ubyte*) 0xBA68A5, MemorySafe.safe);
	immutable state = readMemory!ubyte(cast(ubyte*) 0xBA67A4, MemorySafe.safe);

	if (state != oldState)
	{
		if (state)
			lockMouse();
		else
			unlockMouse();
	}

	if (menuId == oldMenuId && state == oldState)
		return;

	if (menuId == 5 && !cmpMemory(0x53EB12, [0x90, 0x90, 0x90, 0x90, 0x90], MemorySafe.safe))
	{
		bak = readMemory(0x53EB12, 5, MemorySafe.safe);
		setMemory(0x53EB12, 0x90, 5, MemorySafe.safe);
	}
	else if (oldMenuId == 5 || !state)
	{
		writeMemory(0x53EB12, bak, MemorySafe.safe);
	}

	oldMenuId = menuId;
	oldState = state;
}

void lockMouse()
{
	// Lock X
	writeMemory(0x53F44C, [0x31, 0xD2, 0x90, 0x90], MemorySafe.safe);
	// Lock Y
	writeMemory(0x53F453, [0x31, 0xC0, 0x90, 0x90], MemorySafe.safe);
}

void unlockMouse()
{
	writeMemory(0x53F44C, [0x8B, 0x54, 0x24, 0x08], MemorySafe.safe);
	writeMemory(0x53F453, [0x8B, 0x44, 0x24, 0x0C], MemorySafe.safe);
}

@INI("Game settings", "game")
struct Config
{
	@INI("Stay on pause when menu is opened") bool pause = true;
	@INI ConfigMap map;
}

@INI("Map settings", "map")
struct ConfigMap
{
	@INI("Transparent map background") bool trans = false;
}
